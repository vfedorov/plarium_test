﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Serializers
{
    interface ISerializer<T>
    {
        string Serialize(T data);
    }
}
