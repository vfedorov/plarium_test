﻿using PlariumTest.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PlariumTest.Serializers
{
    class NodeXMLSerializer : ISerializer<NodeData>
    {
        // VF: don't simplify to Serialize(object).
        // Idea here is to replace reference at any moment to any serializer, which can require direct acees to NodeData's fields
        public string Serialize(NodeData data)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(data.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, data);
                return textWriter.ToString();
            }
        }
    }
}
