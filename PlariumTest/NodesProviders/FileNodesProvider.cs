﻿using PlariumTest.Data;
using PlariumTest.Loggers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.NodesProviders
{
    class FileNodesProvider : INodesProvider
    {
        // factory
        private NodeDataFactory _nodeDataFactory;

        // injected references
        private ILogger _logger;

        public FileNodesProvider(ILogger logger)
        {
            _logger = logger;
            _nodeDataFactory = new NodeDataFactory(_logger);
        }

        public IEnumerable<NodeData> GetNodesForDirectory(string path)
        {
            var rootDirectory = new DirectoryInfo(path);
            foreach (var node in CreateDirectoryNode(rootDirectory))
            {
                yield return node;
            }
        }

        private IEnumerable<NodeData> CreateDirectoryNode(DirectoryInfo directoryInfo, NodeData parent = null)
        {
            var directoryNode = _nodeDataFactory.GetNodeData(directoryInfo);
            directoryNode.Parent = parent;

            if (parent != null)
            {
                parent.Children.Add(directoryNode);
            }

            yield return directoryNode;

            // check for access (folder can be ReadOnly)
            IEnumerable<FileInfo> filesEnumerable;

            try
            {
                filesEnumerable = directoryInfo.EnumerateFiles();
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to enumerate directory \"{0}\" : {1}", directoryInfo.FullName, e.Message));
                yield break;
            }

            // return files first
            foreach (var file in filesEnumerable)
            {
                var fileNode = _nodeDataFactory.GetNodeData(file);
                fileNode.Parent = directoryNode;

                directoryNode.Children.Add(fileNode);
                yield return fileNode;
            }

            // recursive return directories
            foreach (var subDirectory in directoryInfo.EnumerateDirectories())
            {
                foreach (var subDirectoryNode in CreateDirectoryNode(subDirectory, directoryNode))
                {
                    yield return subDirectoryNode;
                }
            }
        }
    }
}
