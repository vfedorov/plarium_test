﻿using PlariumTest.Data;
using PlariumTest.Loggers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.NodesProviders
{
    class NodeDataFactory
    {
        // show only this rights - customer wish in gdd. Change to any you need
        private FileSystemRights _rightsMask = FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.Delete;

        // injected references
        private ILogger _logger;

        public NodeDataFactory(ILogger logger)
        {
            _logger = logger;
        }

        public NodeData GetNodeData(DirectoryInfo info)
        {
            var nodeData = this.GetNodeData((FileSystemInfo)info);

            nodeData.NodeType = NodeData.ENodeType.Directory;

            nodeData.Size = 0; // TODO: add directory size calculation
            
            // rights
            try
            {
                var security = info.GetAccessControl();
                nodeData.Rights = GetRights(security, _rightsMask);
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to get rights for folder \"{0}\" : {1}", info.FullName, e.Message));
            }

            return nodeData;
        }
        
        public NodeData GetNodeData(FileInfo info)
        {
            var nodeData = this.GetNodeData((FileSystemInfo)info);

            nodeData.NodeType = NodeData.ENodeType.File;

            nodeData.Size = info.Length;

            // rights
            try
            {
                var security = info.GetAccessControl();
                nodeData.Rights = GetRights(security, _rightsMask);
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to get rights for file \"{0}\" : {1}", info.FullName, e.Message));
            }

            return nodeData;
        }

        private NodeData GetNodeData(FileSystemInfo info)
        {
            var nodeData = new NodeData()

            {
                Name = info.Name,
                CreationTime = info.CreationTime,
                LastAccessTime = info.LastAccessTime,
                LastWriteTime = info.LastWriteTime,
                Attributes = info.Attributes.ToString(),
            };

            try
            {
                nodeData.Owner = System.IO.File.GetAccessControl(info.FullName).GetOwner(typeof(System.Security.Principal.NTAccount)).ToString();
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to get owner for \"{0}\" : {1}", info.FullName, e.Message));
                nodeData.Owner = "-";
            }

            return nodeData;
        }

        private string GetRights(CommonObjectSecurity security, FileSystemRights mask)
        {
            string rights = string.Empty;

            FileSystemRights combinedRights = (FileSystemRights)0;

            foreach (FileSystemAccessRule rule in security.GetAccessRules(true, true, typeof(NTAccount)))
            {
                combinedRights |= rule.FileSystemRights;
            }
            
            rights = (combinedRights & mask).ToString();

            return rights;
        }
    }
}
