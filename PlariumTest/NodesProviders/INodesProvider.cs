﻿using PlariumTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.NodesProviders
{
    interface INodesProvider
    {
        IEnumerable<NodeData> GetNodesForDirectory(string path);
    }
}
