﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PlariumTest.Data
{
    [Serializable]
    public class NodeData
    {
        public ENodeType NodeType { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastWriteTime { get; set; }
        public DateTime LastAccessTime { get; set; }
        public string Attributes { get; set; }
        public long Size { get; set; }
        public string Owner { get; set; }
        public string Rights { get; set; }

        [XmlIgnore]
        public List<NodeData> Children { get; set; } = new List<NodeData>();
        [XmlIgnore]
        public NodeData Parent { get; set; }

        public NodeData()
        {

        }

        public enum ENodeType
        {
            File,
            Directory
        }
    }
}
