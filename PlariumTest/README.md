# plarium_test

some code comments:

- Architecture:
Using some type of dependency injection, 
trying maximally to separate functionality by different classes, so it can be easy tested or replaced in future

- So where is some DI library?
For such small project i decided just manually to inject via constructors and don't spend time for additional setup

- What about tests?
I maximally prepared classes, so it can be easy tested:
F.e., you can easy to change INodesProvider to some provider without FileSystem, just raw numbers - and test tree drawing on it.
 But I hadn't time to add tests.

- Threading
Yes, here we havn't fixed 3 threads. Instead of this, I created 3 ThreadRunners:
1 read and provides data in alone thread during one collecting process,
and 2 of them are ListenerThreadRunnerBase with queue of processed objects -
so they are starting thread only when have some data to process.
I hope, this provides more independence and stability for this tasks, 
so they don't need to take care of started/finished main CollectDataThread - 
they are just doing their work when some data is added

- Exceptions
Becaue of such simple type of logging, I just show exception message for any exception, 
I decided not to separate catch Syste.Exception into whole stack of IOException, ArgumentException, UnauthorizedAccessException, etc

- Spent time
I had time to work only at night after work, so in total I spent something like 4 days x3.5 h = 14h
Of cource this is rather more, then estimated 5h in gdd, 
but I was trying to make everything in maxamally neatly way with log, pause/resume and everything else, that came to mind during development process 

- Environment
I was using VS 2017 with .NET 4.6.1. It's difference from requirments in gdd. But it just was allready installed on my work pc.
If it's not ok for you - please let me know, and I'll spend some time to rebuild project on required configurations

P.S. if you have some doubts - feel free to ask me to add/edit something: git is always opened for new commits :)
It was interesting for me to develop this project

Best regards, Vladimir