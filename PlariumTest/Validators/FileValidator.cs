﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Validators
{
    class FileValidator : IValidator
    {
        public readonly string cEmptyInput = "Path can't be empty";
        public readonly string cNotExistsFormat = "\"{0}\" not exists";

        public FileValidator()
        {

        }

        public bool ValidateDirectory(string path, string errorPrefix, out string error)
        {
            error = string.Empty;

            if (string.IsNullOrEmpty(path))
            {
                error = errorPrefix + cEmptyInput;
                return false;
            } 

            if(!Directory.Exists(path))
            {
                error = errorPrefix + string.Format(cNotExistsFormat, path);
                return false;
            }

            return true;
        }

        public bool ValidateFile(string path, string errorPrefix, out string error)
        {
            error = string.Empty;

            if (string.IsNullOrEmpty(path))
            {
                error = errorPrefix + cEmptyInput;
                return false;
            }

            if (!File.Exists(path))
            {
                error = errorPrefix + string.Format(cNotExistsFormat, path);
                return false;
            }

            return true;
        }
    }
}
