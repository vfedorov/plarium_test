﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Validators
{
    interface IValidator
    {
        bool ValidateDirectory(string path, string errorPrefix, out string error);
        bool ValidateFile(string path, string errorPrefix, out string error);
    }
}
