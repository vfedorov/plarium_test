﻿using PlariumTest.Data;
using PlariumTest.DataWriters;
using PlariumTest.Formatters;
using PlariumTest.Loggers;
using PlariumTest.NodesProviders;
using PlariumTest.Serializers;
using PlariumTest.Threads;
using PlariumTest.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlariumTest
{
    public partial class Form1 : Form
    {
        // threads
        private CollectDataThreadRunner _collectDataThreadOwner;
        private DrawDataThreadRunner _drawDataThreadOwner;
        private SerializeDataThreadRunner _serializeDataThreadRunner;

        private List<IPausable> _pausables = new List<IPausable>();

        // references to inject
        private INodesProvider _nodesProvider;
        private IDataFormatter _dataFormatter = new DataFormatter(DataFormatter.ESizeDisplayType.Best);
        private ISerializer<NodeData> _serializer = new NodeXMLSerializer();
        private IDataWriter _fileDataWriter;
        private IValidator _validator = new FileValidator();
        private ILogger _logger;

        // main process ui state
        private EButtonProcessState _processState;

        public Form1()
        {
            InitializeComponent();
            InitializeDependencies();
            SetInitialUIData();
            InitializeThreads();           
        }

        ////////////////////////////////////
        #region Initialize

        private void InitializeDependencies()
        {
            _logger = new TextBoxLogger(this.rtbLog);
            _nodesProvider = new FileNodesProvider(_logger);
            _fileDataWriter = new FileDataWriter(_logger, _validator);
        }

        private void SetInitialUIData()
        {
            // set as start current project's dir
            txtScanFolder.Text = @"../..";
            this.numUpDownDelay.Value = 100;
            SetButtonProcessState(EButtonProcessState.ToProcess);
        }

        private void InitializeThreads()
        {
            // create threads
            _collectDataThreadOwner = new CollectDataThreadRunner(_logger, _nodesProvider);
            _drawDataThreadOwner = new DrawDataThreadRunner(_logger, this.treeListView1, _dataFormatter);
            _serializeDataThreadRunner = new SerializeDataThreadRunner(_logger, _serializer, _fileDataWriter);

            // setup their synchronization
            _collectDataThreadOwner.AddListener(_drawDataThreadOwner);
            _collectDataThreadOwner.AddListener(_serializeDataThreadRunner);

            // combine to easy call pausable
            _pausables.Add(_collectDataThreadOwner);
            _pausables.Add(_drawDataThreadOwner);
            _pausables.Add(_serializeDataThreadRunner);

            // subsribe to threads finish to allow restart process
            // TODO: subsribe to all threads finish, cause some of them can continue working after collection finish
            _collectDataThreadOwner.CollectionFinished += _collectDataThreadOwner_CollectionFinished;
        }

        #endregion ICollectDataListener
        ////////////////////////////////////

        ////////////////////////////////////
        #region Handlers

        private void _collectDataThreadOwner_CollectionFinished()
        {
            btnProcess.Invoke(new Action(() =>
            {
                SetButtonProcessState(EButtonProcessState.ToProcess);
                _logger.LogInfo("Completed!");
            }));

            MessageBox.Show("Completed!");
        }

        private void btnScanFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtScanFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnOutputXml_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtOutputXml.Text = openFileDialog1.FileName;
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            switch(_processState)
            {
                case EButtonProcessState.ToProcess:
                    {
                        if (!ValidateInput())
                        {
                            break;
                        }

                        SetButtonProcessState(EButtonProcessState.ToPause);
                        _fileDataWriter.Path = this.txtOutputXml.Text;
                        _collectDataThreadOwner.Start(new CollectDataSettings() { Path = this.txtScanFolder.Text, Delay = this.numUpDownDelay.Value });
                        _logger.LogInfo(string.Format("Start process Folder: \"{0}\"; out XML: \"{1}\"", txtScanFolder.Text, txtOutputXml.Text));
                        break;
                    }
                case EButtonProcessState.ToPause:
                    {
                        _logger.LogInfo("Paused");
                        SetButtonProcessState(EButtonProcessState.ToResume);
                        foreach (var pausable in _pausables)
                        {
                            pausable.Pause();
                        }
                        break;
                    }
                case EButtonProcessState.ToResume:
                    {
                        _logger.LogInfo("Resumed");
                        SetButtonProcessState(EButtonProcessState.ToPause);
                        foreach (var pausable in _pausables)
                        {
                            pausable.Resume();
                        }
                        break;
                    }
            }
        }

        #endregion Handlers
        ////////////////////////////////////

        ////////////////////////////////////
        #region ButtonProcessState

        private void SetButtonProcessState(EButtonProcessState state)
        {
            _processState = state;
            switch (state)
            {
                case EButtonProcessState.ToProcess:
                    btnProcess.Text = "Process";
                    break;
                case EButtonProcessState.ToPause:
                    btnProcess.Text = "Pause";
                    break;
                case EButtonProcessState.ToResume:
                    btnProcess.Text = "Resume";
                    break;
            }
        }
        
        private enum EButtonProcessState
        {
            ToProcess,
            ToPause,
            ToResume
        }

        #endregion ICollectDataListener
        ////////////////////////////////////

        ////////////////////////////////////
        #region Validation

        private bool ValidateInput()
        {
            string error;

            if (!_validator.ValidateDirectory(this.txtScanFolder.Text, "Output Folder error: ", out error))
            {
                _logger.LogError(error);
                MessageBox.Show(error);

                return false;
            }

            if (!_validator.ValidateFile(this.txtOutputXml.Text, "Output XML file error: ", out error))
            {
                _logger.LogError(error);

                // allow user to process without
                DialogResult dialogResult = MessageBox.Show(error,"Continue without XML output?" , MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // VF: HACK! clear txtOutputXml.Text, so it will not be processed in SerializeDataThread
                    // It's now less priority to make some more neatly solution
                    this.txtOutputXml.Text = string.Empty;
                    return true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    _logger.LogError("Cancelled");
                    return false;
                }
                
                return false;
            }

            return true;
        }

        #endregion ICollectDataListener
        ////////////////////////////////////
    }
}
