﻿using System.Windows.Forms;

namespace PlariumTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtScanFolder = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnScanFolder = new System.Windows.Forms.Button();
            this.panelScanFolder = new System.Windows.Forms.Panel();
            this.panelSelectXML = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOutputXml = new System.Windows.Forms.TextBox();
            this.btnOutputXml = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.treeListView1 = new BrightIdeasSoftware.TreeListView();
            this.panelDelay = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.numUpDownDelay = new System.Windows.Forms.NumericUpDown();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.panelScanFolder.SuspendLayout();
            this.panelSelectXML.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).BeginInit();
            this.panelDelay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "folder.png");
            this.imageList1.Images.SetKeyName(1, "file.png");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Scan folder:";
            // 
            // txtScanFolder
            // 
            this.txtScanFolder.Location = new System.Drawing.Point(73, 8);
            this.txtScanFolder.Name = "txtScanFolder";
            this.txtScanFolder.Size = new System.Drawing.Size(100, 20);
            this.txtScanFolder.TabIndex = 2;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnScanFolder
            // 
            this.btnScanFolder.Location = new System.Drawing.Point(179, 7);
            this.btnScanFolder.Name = "btnScanFolder";
            this.btnScanFolder.Size = new System.Drawing.Size(20, 20);
            this.btnScanFolder.TabIndex = 4;
            this.btnScanFolder.Text = "...";
            this.btnScanFolder.UseVisualStyleBackColor = true;
            this.btnScanFolder.Click += new System.EventHandler(this.btnScanFolder_Click);
            // 
            // panelScanFolder
            // 
            this.panelScanFolder.Controls.Add(this.label1);
            this.panelScanFolder.Controls.Add(this.txtScanFolder);
            this.panelScanFolder.Controls.Add(this.btnScanFolder);
            this.panelScanFolder.Location = new System.Drawing.Point(0, 1);
            this.panelScanFolder.Name = "panelScanFolder";
            this.panelScanFolder.Size = new System.Drawing.Size(266, 38);
            this.panelScanFolder.TabIndex = 5;
            // 
            // panelSelectXML
            // 
            this.panelSelectXML.Controls.Add(this.label2);
            this.panelSelectXML.Controls.Add(this.txtOutputXml);
            this.panelSelectXML.Controls.Add(this.btnOutputXml);
            this.panelSelectXML.Location = new System.Drawing.Point(272, 1);
            this.panelSelectXML.Name = "panelSelectXML";
            this.panelSelectXML.Size = new System.Drawing.Size(266, 38);
            this.panelSelectXML.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Output XML:";
            // 
            // txtOutputXml
            // 
            this.txtOutputXml.Location = new System.Drawing.Point(73, 8);
            this.txtOutputXml.Name = "txtOutputXml";
            this.txtOutputXml.Size = new System.Drawing.Size(100, 20);
            this.txtOutputXml.TabIndex = 2;
            // 
            // btnOutputXml
            // 
            this.btnOutputXml.Location = new System.Drawing.Point(179, 7);
            this.btnOutputXml.Name = "btnOutputXml";
            this.btnOutputXml.Size = new System.Drawing.Size(20, 20);
            this.btnOutputXml.TabIndex = 4;
            this.btnOutputXml.Text = "...";
            this.btnOutputXml.UseVisualStyleBackColor = true;
            this.btnOutputXml.Click += new System.EventHandler(this.btnOutputXml_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(713, 1);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 38);
            this.btnProcess.TabIndex = 7;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // treeListView1
            // 
            this.treeListView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeListView1.CellEditUseWholeCell = false;
            this.treeListView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView1.LargeImageList = this.imageList1;
            this.treeListView1.Location = new System.Drawing.Point(0, 45);
            this.treeListView1.Name = "treeListView1";
            this.treeListView1.ShowGroups = false;
            this.treeListView1.Size = new System.Drawing.Size(800, 288);
            this.treeListView1.SmallImageList = this.imageList1;
            this.treeListView1.TabIndex = 0;
            this.treeListView1.UseCompatibleStateImageBehavior = false;
            this.treeListView1.View = System.Windows.Forms.View.Details;
            this.treeListView1.VirtualMode = true;
            // 
            // panelDelay
            // 
            this.panelDelay.Controls.Add(this.label3);
            this.panelDelay.Controls.Add(this.numUpDownDelay);
            this.panelDelay.Location = new System.Drawing.Point(544, 1);
            this.panelDelay.Name = "panelDelay";
            this.panelDelay.Size = new System.Drawing.Size(163, 38);
            this.panelDelay.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Delay:";
            // 
            // numUpDownDelay
            // 
            this.numUpDownDelay.Location = new System.Drawing.Point(40, 9);
            this.numUpDownDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numUpDownDelay.Name = "numUpDownDelay";
            this.numUpDownDelay.Size = new System.Drawing.Size(120, 20);
            this.numUpDownDelay.TabIndex = 3;
            // 
            // rtbLog
            // 
            this.rtbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbLog.Location = new System.Drawing.Point(0, 339);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.ReadOnly = true;
            this.rtbLog.Size = new System.Drawing.Size(800, 110);
            this.rtbLog.TabIndex = 8;
            this.rtbLog.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.rtbLog);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.panelDelay);
            this.Controls.Add(this.panelScanFolder);
            this.Controls.Add(this.panelSelectXML);
            this.Controls.Add(this.treeListView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panelScanFolder.ResumeLayout(false);
            this.panelScanFolder.PerformLayout();
            this.panelSelectXML.ResumeLayout(false);
            this.panelSelectXML.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).EndInit();
            this.panelDelay.ResumeLayout(false);
            this.panelDelay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownDelay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList1;
        private Label label1;
        private TextBox txtScanFolder;
        private OpenFileDialog openFileDialog1;
        private FolderBrowserDialog folderBrowserDialog1;
        private Button btnScanFolder;
        private Panel panelScanFolder;
        private Panel panelSelectXML;
        private Label label2;
        private TextBox txtOutputXml;
        private Button btnOutputXml;
        private Button btnProcess;
        private BrightIdeasSoftware.TreeListView treeListView1;
        private Panel panelDelay;
        private Label label3;
        private NumericUpDown numUpDownDelay;
        private RichTextBox rtbLog;
    }
}

