﻿using PlariumTest.Loggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlariumTest.Threads
{
    /// <summary>
    /// Base class for all ThreadRunners - contains logic to work with thread
    /// </summary>
    abstract class ThreadRunnerBase : IPausable
    {
        // threading
        private Thread _thread;
        private ManualResetEvent _pausingEvent = new ManualResetEvent(true);

        // injected references
        protected ILogger _logger; 

        public ThreadRunnerBase(ILogger logger)
        {
            _logger = logger;
        }

        ////////////////////////////////////
        #region IPausable

        public void Pause()
        {
            _pausingEvent.Reset();
        }

        public void Resume()
        {
            _pausingEvent.Set();
        }

        #endregion IPausable
        ////////////////////////////////////

        protected abstract void ThreadFunction();

        protected void WaitIfThreadPaused()
        {
            _pausingEvent.WaitOne();
        }

        protected void CreateNewThread(bool start)
        {
            _thread = new Thread(ThreadFunction);
            //_thread.IsBackground = true; // stop threads on close main forms
            // VF: us IsBackground = true blocking ui buttons responce. so, for now, leave it unused
            if (start)
            {
                StartThread();
            }
        }

        protected void StartThread()
        {
            _thread.Start();
        }

        protected bool IsThreadWorkingNow
        {
            get
            {
                return _thread != null && _thread.IsAlive;
            }
        }
    }
}
