﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightIdeasSoftware;
using PlariumTest.Data;
using PlariumTest.Formatters;
using PlariumTest.Loggers;

namespace PlariumTest.Threads
{
    class DrawDataThreadRunner : ListenerThreadRunnerBase
    {
        // data
        private TreeListView _treeListView;
        private List<NodeData> _roots = new List<NodeData>();

        // injected references
        private IDataFormatter _dataFormatter;

        public DrawDataThreadRunner(ILogger logger, TreeListView treeListView, IDataFormatter dataFormatter) : base(logger)
        {
            _treeListView = treeListView;
            _dataFormatter = dataFormatter;

            // VF: leave init colums code here for now just to keep all code for tree in one place;
            // think about better separate ui and processing
            CreateAndLinkColums(_dataFormatter);

            _treeListView.Expanded += _treeListView_Expanded;
        }

        ////////////////////////////////////
        #region Implementation

        protected override void ProcessNextNodeFromQueue(NodeData node)
        {
            _treeListView.Invoke(new Action(()=>
            {
                if (node.Parent == null)
                {
                    _treeListView.AddObject(node);
                }
                else
                {
                    if (!_treeListView.IsExpanded(node.Parent))
                    {
                        _treeListView.Expand(node.Parent);
                    }
                }

                // leave RebuildAll cause didn't found some another working variant: UpdateObject gives some unpredictable result
                _treeListView.RebuildAll(true);

                if (node.NodeType == NodeData.ENodeType.Directory)
                {
                    _treeListView.Expand(node);
                }

            }), new object[] { });
        }

        ////////////////
        #region UI

        private void CreateAndLinkColums(IDataFormatter dataFormatter)
        {
            // set the delegate that the tree uses to know if a node is expandable
            _treeListView.CanExpandGetter = x => (x as NodeData).Children.Count > 0;
            // set the delegate that the tree uses to know the children of a node
            _treeListView.ChildrenGetter = x => (x as NodeData).Children;
            // set the delegate that the tree uses to know the parent of a node
            _treeListView.ParentGetter = x => (x as NodeData).Parent;

            // clear colums just in case
            _treeListView.Columns.Clear();

            // create the tree columns and set the delegates to print the desired object proerty
            AddColum(_treeListView, "Name", x => (x as NodeData).Name);
            AddColum(_treeListView, "Created", x => dataFormatter.FormatDateTime((x as NodeData).CreationTime));
            AddColum(_treeListView, "Modified", x => dataFormatter.FormatDateTime((x as NodeData).LastWriteTime));
            AddColum(_treeListView, "LastAccess", x => dataFormatter.FormatDateTime((x as NodeData).LastAccessTime));
            AddColum(_treeListView, "Attributes", x => (x as NodeData).Attributes);
            AddColum(_treeListView, "Size", x => dataFormatter.FormatSize((x as NodeData).Size));
            AddColum(_treeListView, "Owner", x => (x as NodeData).Owner);
            AddColum(_treeListView, "Rights", x => (x as NodeData).Rights);

            this._treeListView.AutoSizeColumns();
            this._treeListView.AutoResizeColumns();
        }

        private void AddColum(TreeListView treeListView, string name, AspectGetterDelegate getter)
        {
            var nameCol = new OLVColumn(name, name);
            nameCol.AspectGetter = getter;
            treeListView.Columns.Add(nameCol);
        }

        private void DrawTree()
        {
            this._treeListView.Roots = _roots;
            
        }

        #endregion UI
        ////////////////

        private void _treeListView_Expanded(object sender, TreeBranchExpandedEventArgs e)
        {
            _treeListView.AutoSizeColumns();
            _treeListView.AutoResizeColumns();
        }

        #endregion Implementation
        ////////////////////////////////////
    }
}
