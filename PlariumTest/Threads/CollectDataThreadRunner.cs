﻿using PlariumTest.Data;
using PlariumTest.Loggers;
using PlariumTest.NodesProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlariumTest.Threads
{
    class CollectDataThreadRunner : ThreadRunnerBase
    {
        // data
        private List<NodeData> _nodes = new List<NodeData>();
        private CollectDataSettings _settings;

        // listeners
        private List<ICollectDataListener> _listeners = new List<ICollectDataListener>();

        // injected references
        private INodesProvider _nodesProvider;

        // events
        // actually, duplicated some functions from ICollectDataListener 
        // to provide alternative subscription way for classes, who don't want to implement interface
        public event Action CollectionStarted;
        public event Action CollectionFinished;

        public CollectDataThreadRunner(ILogger logger, INodesProvider nodesProvider) : base(logger)
        {
            _nodesProvider = nodesProvider;
        }

        public void Start(CollectDataSettings settings)
        {
            _settings = settings;
            CreateNewThread(true);
        }
               

        ////////////////////////////////////
        #region Listeners

        public void AddListener(ICollectDataListener listener)
        {
            Monitor.Enter(_listeners);

            if (!_listeners.Contains(listener))
            {
                _listeners.Add(listener);
            }

            Monitor.Exit(_listeners);
        }

        public void RemoveListener(ICollectDataListener listener)
        {
            Monitor.Enter(_listeners);

            _listeners.Remove(listener);

            Monitor.Exit(_listeners);
        }

        #endregion Listeners
        ////////////////////////////////////

        ////////////////////////////////////
        #region Implementation

        protected override void ThreadFunction()
        {
            ListDirectory(_nodes, _settings.Path);
        }

        private void ListDirectory(List<NodeData> nodes, string path)
        {
            // notofy listeners about start
            NotifyCollectionStarted();

            nodes.Clear();
                        
            foreach (var node in _nodesProvider.GetNodesForDirectory(path))
            {
                // pause thread if pause event is set by somebody
                WaitIfThreadPaused();
                
                // save node
                nodes.Add(node);

                // notify listeners about new node
                NotifyCollectedNode(node);

                // wait if delay is set
                if (_settings.Delay > 0)
                {
                    Thread.Sleep((int)_settings.Delay);
                }
            }

            // that's all - otify listeners
            NotifyCollectionFinished();
        }


        //////////////////////////
        #region NotifyListeners

        private void NotifyCollectionStarted()
        {
            Monitor.Enter(_listeners);
            try
            {
                foreach (var listener in _listeners)
                {
                    listener.CollectionStarted();
                }

                if(CollectionStarted != null)
                {
                    CollectionStarted();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("Error during collection data Start: {0} \n {1} \n", e.Message, e.StackTrace));
            }
            finally
            {
                Monitor.Exit(_listeners);
            }
        }

        private void NotifyCollectedNode(NodeData node)
        {
            Monitor.Enter(_listeners);
            try
            {
                foreach (var listener in _listeners)
                {
                    listener.CollectedNode(node);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("Error during collected node callback: {0} \n {1} \n", e.Message, e.StackTrace));
            }
            finally
            {
                Monitor.Exit(_listeners);
            }
        }

        private void NotifyCollectionFinished()
        {
            Monitor.Enter(_listeners);
            try
            {
                foreach (var listener in _listeners)
                {
                    listener.CollectionFinished();
                }

                if (CollectionFinished != null)
                {
                    CollectionFinished();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("Error during collection data Finish: {0} \n {1} \n", e.Message, e.StackTrace));
            }
            finally
            {
                Monitor.Exit(_listeners);
            }
        }

        #endregion NotifyListeners
        //////////////////////////

        #endregion Implementation
        ////////////////////////////////////
    }

    struct CollectDataSettings
    {
        public string Path;
        public decimal Delay;
    }
}
