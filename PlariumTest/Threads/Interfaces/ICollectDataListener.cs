﻿using PlariumTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Threads
{
    interface ICollectDataListener
    {
        void CollectionStarted();
        void CollectedNode(NodeData node);
        void CollectionFinished();
    }
}
