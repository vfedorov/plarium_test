﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Threads
{
    interface IPausable
    {
        void Pause();
        void Resume();
    }
}
