﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PlariumTest.Data;
using PlariumTest.Loggers;

namespace PlariumTest.Threads
{
    /// <summary>
    /// Base class for all ThreadRunners, containts logic to process data collection result by one item
    /// </summary>
    abstract class ListenerThreadRunnerBase : ThreadRunnerBase, ICollectDataListener
    {
        // data
        private Queue<NodeData> _nodesQueue = new Queue<NodeData>();

        public ListenerThreadRunnerBase(ILogger logger) : base(logger)
        {
            
        }

        ////////////////////////////////////
        #region ICollectDataListener

        public virtual void CollectionStarted()
        {
            
        }

        public virtual void CollectedNode(NodeData node)
        {
            Monitor.Enter(_nodesQueue);

            _nodesQueue.Enqueue(node);

            Monitor.Exit(_nodesQueue);

            if (!IsThreadWorkingNow)
            {
                CreateNewThread(true);
            }
        }

        public virtual void CollectionFinished()
        {
            
        }

        #endregion ICollectDataListener
        ////////////////////////////////////

        ////////////////////////////////////
        #region Implementation

        protected override void ThreadFunction()
        {
            WaitIfThreadPaused();

            // lock queue for get count and dequeue
            Monitor.Enter(_nodesQueue);
            while(_nodesQueue.Count > 0)
            {
                WaitIfThreadPaused();

                var node = _nodesQueue.Dequeue();

                // unlock queue - we'll process node for some time
                Monitor.Exit(_nodesQueue);

                ProcessNextNodeFromQueue(node);

                // re-lock queue for next iteration
                Monitor.Enter(_nodesQueue);
            }
            
            // unlock queue before exit
            Monitor.Exit(_nodesQueue);
        }

        protected abstract void ProcessNextNodeFromQueue(NodeData node);
        
        #endregion Implementation
        ////////////////////////////////////
    }
}
