﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BrightIdeasSoftware;
using PlariumTest.Data;
using PlariumTest.DataWriters;
using PlariumTest.Formatters;
using PlariumTest.Loggers;
using PlariumTest.Serializers;

namespace PlariumTest.Threads
{
    class SerializeDataThreadRunner : ListenerThreadRunnerBase
    {
        // injected references
        private ISerializer<NodeData> _serializer;
        private IDataWriter _dataWriter;

        // threading
        private object _fileWriteSyncObj = new object();
        

        public SerializeDataThreadRunner(ILogger logger, ISerializer<NodeData> serializer, IDataWriter dataWriter) : base(logger)
        {
            _serializer = serializer;
            _dataWriter = dataWriter;
        }

        public override void CollectionStarted()
        {
            _dataWriter.ClearFile();
        }

        ////////////////////////////////////
        #region Implementation

        protected override void ProcessNextNodeFromQueue(NodeData node)
        {
            Monitor.Enter(_fileWriteSyncObj);
            try
            {
                string text = _serializer.Serialize(node);
                _dataWriter.WriteString(text);
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to serialize node \"{0}\" : {1}", node.Name, e.Message));
            }
            finally
            {
                Monitor.Exit(_fileWriteSyncObj);
            }
        }

        #endregion Implementation
        ////////////////////////////////////
    }
}
