﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlariumTest.Loggers
{
    class TextBoxLogger : ILogger
    {
        public RichTextBox TextBox { get; private set; }

        public TextBoxLogger(RichTextBox textBox)
        {
            this.TextBox = textBox;
        }

        public void LogInfo(string message)
        {
            this.TextBox.Invoke(new Action(() =>
            {
                this.TextBox.SelectionColor = Color.Green;
                this.TextBox.SelectedText = Environment.NewLine + message;
            }));
        }

        public void LogWarning(string message)
        {
            this.TextBox.Invoke(new Action(() =>
            {
                this.TextBox.SelectionColor = Color.Orange;
                this.TextBox.SelectedText = Environment.NewLine + message;
            }));
        }

        public void LogError(string message)
        {
            this.TextBox.Invoke(new Action(() =>
            {
                this.TextBox.SelectionColor = Color.Red;
                this.TextBox.SelectedText = Environment.NewLine + message;
            }));
        }
    }
}
