﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Formatters
{
    class DataFormatter : IDataFormatter
    {
        public ESizeDisplayType SizeDisplayType { get; set; }

        public DataFormatter(ESizeDisplayType sizeDisplayType)
        {
            SizeDisplayType = sizeDisplayType;
        }

        public string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString();
        }

        public string FormatSize(long size)
        {
            return FormatSize(size, this.SizeDisplayType);
        }

        private string FormatSize(long size, ESizeDisplayType displayType)
        {
            double gb = 1024 * 1024 * 1024;
            double mb = 1024 * 1024;
            double kb = 1024;

            if (displayType == ESizeDisplayType.Bytes)
                return string.Format("{0} bytes", size);
            if (displayType == ESizeDisplayType.KBytes)
                return string.Format("{0} KB", (int)Math.Ceiling((double)size / kb));
            if (displayType == ESizeDisplayType.MBytes)
            {
                double dsize = Math.Round((double)size / mb, 2);
                return string.Format("{0:f2} MB", dsize);
            }
            if (displayType == ESizeDisplayType.Best)
            {
                long sizekb = (long)Math.Ceiling((double)size / kb);
                if (size <= 100)
                    return string.Format("{0} KB", sizekb);

                if (size > (100 * gb))
                    return string.Format("{0:f2} GB", Math.Round((double)size / gb, 2));
                if (size > (100 * mb))
                    return string.Format("{0:f2} MB", Math.Round((double)size / mb, 2));
                return string.Format("{0} KB", (int)Math.Ceiling(size / kb));
            }
            return string.Format("{0} KB", (int)Math.Ceiling(size / kb));
        }

        public enum ESizeDisplayType
        {
            Bytes,
            KBytes,
            MBytes,
            Best,
        }
    }
}
