﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.Formatters
{
    interface IDataFormatter
    {
        string FormatDateTime(DateTime dateTime);
        string FormatSize(long size);
    }
}
