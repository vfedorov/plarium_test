﻿using PlariumTest.Loggers;
using PlariumTest.Validators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlariumTest.DataWriters
{
    class FileDataWriter : IDataWriter
    {
        // data
        private string _path;
        public string Path
        {
            get => _path;
            set
            {
                if (_path != value)
                {
                    _path = value;
                    // validate with log to see messages right on set value, if something wrong
                    ValidatePath(true);
                }
            }
        }

        // threading
        private object _fileWriteSyncObj = new object();

        // injected references
        private ILogger _logger;
        private IValidator _validator;

        public FileDataWriter(ILogger logger, IValidator validator)
        {
            _logger = logger;
            _validator = validator;
        }

        public FileDataWriter(string path)
        {
            Path = path;
        }

        public void ClearFile()
        {
            if (!ValidatePath())
            {
                return;
            }

            Monitor.Enter(_fileWriteSyncObj);
            try
            {
                File.WriteAllText(_path, string.Empty);
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to clear file \"{0}\" : {1}", Path, e.Message));
            }
            finally
            {
                Monitor.Exit(_fileWriteSyncObj);
            }
        }

        public void WriteString(string data)
        {
            if (!ValidatePath())
            {
                return;
            }

            Monitor.Enter(_fileWriteSyncObj);
            try
            {
                File.AppendAllText(_path, data + Environment.NewLine);
            }
            catch (Exception e)
            {
                _logger.LogWarning(string.Format("Failed to write to file \"{0}\" : {1}", Path, e.Message));
            }
            finally
            {
                Monitor.Exit(_fileWriteSyncObj);
            }
        }

        private bool ValidatePath(bool showLog = false)
        {
            string error;
            if (!_validator.ValidateFile(_path, "", out error))
            {
                if (showLog)
                {
                    _logger.LogError("Write data to file will be skiped! " + error);
                }
                return false;
            }

            return true;
        }
    }
}
