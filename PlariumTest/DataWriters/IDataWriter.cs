﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTest.DataWriters
{
    interface IDataWriter
    {
        string Path { get; set; }
        void ClearFile();
        void WriteString(string data);
    }
}
